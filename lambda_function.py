"""update or create DNS entry from tag URL """
from __future__ import print_function

import os
import re
import boto3

HOSTED_ZONE_ID = os.environ.get('HOSTED_ZONE_ID')
HOSTED_ZONE_ADDRESS = os.environ.get('HOSTED_ZONE_ADDRESS')

HOSTED_ZONE_ADDRESS = HOSTED_ZONE_ADDRESS[:-1] \
    if HOSTED_ZONE_ADDRESS[-1] == '.' else HOSTED_ZONE_ADDRESS


def search(dicts, search_for):
    """search function"""
    for item in dicts:
        if search_for == item['Key']:
            return item['Value']
    return None


def is_valid_hostname(hostname):
    """Check hostname value"""
    if len(hostname) > 255:
        return False
    if hostname[-1] == ".":
        hostname = hostname[:-1]
    allowed = re.compile("(?!-)[A-Z\d-]{1,63}(?<!-)$", re.IGNORECASE)
    return all(allowed.match(x) for x in hostname.split("."))


def lambda_handler(event, context):
    """"create DNS entry"""
    ec2 = boto3.resource('ec2')
    route53 = boto3.client('route53')

    instance_id = event['detail']['instance-id']
    instance = ec2.Instance(instance_id)
    instance_ip = instance.public_ip_address
    url = search(instance.tags, 'URL')
    instance_name = (url
                     if url is not None
                     else 'public-dns-{}'.format(search(instance.tags, 'Name'))).lower()

    print("Processing: {0}".format(instance_id))

    if not is_valid_hostname("{0}.atomic.aws.".format(instance_name)):
        print("Invalid hostname! No changes made.")
        return {'status': "Invalid hostname"}

    dns_changes = {
        'Changes': [
            {
                'Action': 'UPSERT',
                'ResourceRecordSet': {
                    'Name': "{0}.{1}".format(instance_name, HOSTED_ZONE_ADDRESS),
                    'Type': 'A',
                    'ResourceRecords': [
                        {
                            'Value': instance_ip
                        }
                    ],
                    'TTL': 30
                }
            }
        ]
    }

    print("Updating Route53 to create:")
    print("{0} IN A {1}".format(instance_name, instance_ip))

    response = route53.change_resource_record_sets(
        HostedZoneId=HOSTED_ZONE_ID,
        ChangeBatch=dns_changes
    )

    return {'status': response['ChangeInfo']['Status']}
